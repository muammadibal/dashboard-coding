import { ISignal } from "./services";

export interface IPostDetail extends ISignal {
    id: string;
}