export interface ISignal {
    signal: AbortController["signal"]
}

export * from './postService'
export * from './userService'
