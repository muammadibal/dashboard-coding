import { FormFeedback, FormGroup, Input as SInput, Label } from 'reactstrap'
import { InputType } from 'reactstrap/types/lib/Input'
import './styles.css'

interface IInput {
    id?: string
    label?: string
    name?: string
    placeholder?: string
    type?: InputType
    invalid?: boolean
    errors?: string
    value?: string
    onChange?: React.ChangeEventHandler<HTMLInputElement> | undefined
}

const Input = ({ id, label = '', name, placeholder, type = 'text', invalid = false, errors, value, onChange }: IInput) => {
    return (
        <FormGroup>
            {label.length > 0 ?
                <Label for={id}>
                    {label}
                </Label> : null}
            <SInput id={id} type={type} name={name} placeholder={placeholder} invalid={invalid} value={value} onChange={onChange} className='input-style' />
            <FormFeedback>
                {errors}
            </FormFeedback>
        </FormGroup>
    )
}

export default Input