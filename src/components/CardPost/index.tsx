/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { Gap } from '..'
import { getPostComments } from '../../redux/actions/postActions'
import { IPost, IPostComments } from '../../redux/slice/postSlice'
import Comments from './Comments'

const CardPost = ({ post, isShowDetail = false, isShowTitle = false, isShowComments = false }: { post: IPost, isShowDetail?: boolean, isShowTitle?: boolean, isShowComments?: boolean }) => {
    const connUser = new AbortController()
    const dispatch = useDispatch()
    const { users } = useSelector((state: {
        user: {
            users: IPostComments[]
        }
    }) => state.user);
    const [postComments, setPostComments] = useState<IPostComments[]>()

    useEffect(() => {
        (async () => {
            const res = await dispatch<any>(getPostComments({ id: `${post.id}`, signal: connUser.signal }))
            setPostComments(res)
        })()
    }, [])

    const userPost = users.find(o => o.id === post.userId)

    return (
        <div
            style={{
                width: '50%'
            }}>
            {isShowTitle ? <h4 style={{
                width: '65.5%',
                marginLeft: 'auto',
            }}>{post.title}</h4> : null}
            <div
                style={{
                    display: 'flex',
                    flexDirection: 'row',
                }}
            >
                <div style={{
                    width: '30%'
                }}>
                    <h4>{userPost?.name}</h4>
                </div>
                <Gap width={30} />
                <div style={{
                    width: '60%'
                }}>
                    <p>{post.body}</p>
                    {isShowComments ? null :
                        <div
                            style={{
                                display: 'flex',
                                flexDirection: 'row',
                                alignContent: 'center',
                                justifyItems: 'center',
                            }}
                        >
                            <Link to={`/dashboard/post/${post.id}/comments`} style={{
                                textDecoration: 'none',
                                color: 'black',
                                display: 'flex',
                                alignContent: 'center',
                                justifyItems: 'center',
                            }}>
                                <i className='bx bx-message-rounded-detail bx-sm'></i>
                                <Gap width={10} />
                                <p>{postComments?.length}</p>
                            </Link>
                            <Gap width={25} />
                            {isShowDetail ? <Link to={`/dashboard/post/${post.id}`} style={{
                                textDecoration: 'none',
                                color: 'black'
                            }}>Detail</Link> : null}
                        </div>}
                </div>
            </div>
            {isShowComments ?
                <div style={{
                    width: '65.5%',
                    marginLeft: 'auto',
                }}>
                    <h6>All Comments</h6>
                    {postComments?.map((val, i) => <Comments comments={val} />)}
                </div>

                : null}
        </div>
    )
}

export default CardPost