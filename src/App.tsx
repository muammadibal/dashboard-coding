import React from 'react';
import { Route, Routes } from 'react-router-dom';
import { Home, Login, MainHome, NotFound, PostDetail, Register, UserDetail } from './pages';
import './App.css'

function App() {
  return (
    <div>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/dashboard" element={<MainHome />} />
        <Route path="/dashboard/post/:id" element={<PostDetail />} />
        <Route path="/dashboard/post/:id/comments" element={<PostDetail />} />
        <Route path="/dashboard/user/:id" element={<UserDetail />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </div>
  );
}

export default App;
