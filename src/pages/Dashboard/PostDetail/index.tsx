/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { getPostDetail } from '../../../redux/actions/postActions';
import { IPost } from '../../../redux/slice/postSlice';
import { CardPost, NavBar } from '../../../components'

const PostDetail = () => {
    const connUser = new AbortController()
    const location = useLocation();
    const dispatch = useDispatch()
    const navigate = useNavigate();
    let { id } = useParams();
    const { postDetail } = useSelector((state: {
        post: {
            postDetail: IPost
        }
    }) => state.post);

    useEffect(() => {
        dispatch<any>(getPostDetail({ id: id!.toString(), signal: connUser.signal }))
    }, [])

    const isUrlPostComments = location.pathname.split('/').includes('comments');

    return (
        <div>
            <NavBar centerTitle='Post Detail' />

            <div
                style={{
                    display: 'flex',
                    alignItems: 'center',
                    flexDirection: 'column',
                }}>
                <div style={{
                    width: '50%',
                    alignSelf: 'center',
                    cursor: 'pointer',
                    marginBottom: '1rem'
                }} onClick={() => {
                    navigate(-1)
                }}>
                    <i className='bx bxs-chevron-left bx-border bxs-lg'></i>
                </div>
                <CardPost post={postDetail} isShowTitle isShowComments={isUrlPostComments} />
            </div>

        </div>
    )
}

export default PostDetail