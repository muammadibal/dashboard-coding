import { axios } from "../config"
import { IPostDetail, ISignal } from "../interface/services"
import { IPost, IPostComments } from "../redux/slice/postSlice"

export const getPostsServices = async ({ signal }: ISignal) => {
    const res = await axios.get(`/posts`)

    return res.data as IPost[]
}

export const getPostDetailServices = async ({ id, signal }: IPostDetail) => {
    const res = await axios.get(`/posts/${id}`)

    return res.data as IPost
}

export const getPostCommentsServices = async ({ id, signal }: IPostDetail) => {
    const res = await axios.get(`/posts/${id}/comments`)

    return res.data as IPostComments[]
}