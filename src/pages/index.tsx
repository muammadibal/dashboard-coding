import Register from "./Register";
import Login from "./Login";
import Home from "./Home";
import NotFound from "./NotFound";
export * from './Dashboard'

export {
    Register,
    Login,
    Home,
    NotFound
}