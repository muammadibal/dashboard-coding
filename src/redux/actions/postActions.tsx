import { IPostDetail, ISignal } from "../../interface/services"
import { getPostCommentsServices, getPostDetailServices, getPostsServices } from "../../services"
import { getPostCommentsActions, getPostDetailActions, getPostsActions } from "../slice/postSlice"

export const getPosts = ({ signal }: ISignal) => async (dispatch: any) => {
    try {
        const posts = await getPostsServices({ signal })
        dispatch(getPostsActions(posts.sort((a, b) => {
            const aValue = a?.body?.toLowerCase()
            const bValue = b?.body?.toLowerCase()
            if (aValue! < bValue!) return -1
            if (aValue! > bValue!) return 1
            return 0
          })))
        return posts
    } catch (error) {
        return error
    }
}

export const getPostDetail = ({ id, signal }: IPostDetail) => async (dispatch: any) => {
    try {
        const post = await getPostDetailServices({ id, signal })
        dispatch(getPostDetailActions(post))
        return post
    } catch (error) {
        return error
    }
}

export const getPostComments = ({ id, signal }: IPostDetail) => async (dispatch: any) => {
    try {
        const postComment = await getPostCommentsServices({ id, signal })
        dispatch(getPostCommentsActions(postComment))
        return postComment
    } catch (error) {
        return error
    }
}