import { axios } from "../config"
import { ISignal, IUserDetail } from "../interface/services"
import { IUser } from "../redux/slice/userSlice"

export const getUsersServices = async ({ signal }: ISignal) => {
    const res = await axios.get(`/users`, {
        signal
    })

    return res.data as IUser[]
}

export const getUserDetailServices = async ({ id, signal }: IUserDetail) => {
    const res = await axios.get(`/users/${id}`, {
        signal
    })

    return res.data as IUser
}