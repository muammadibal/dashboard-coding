import MainHome from "./MainHome";
import PostDetail from "./PostDetail";
import UserDetail from "./UserDetail";

export {
    MainHome,
    PostDetail,
    UserDetail
}