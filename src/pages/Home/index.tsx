import React from 'react'
import { NavBar } from '../../components'
import WorkSolution from '../../assets/img/work-solution.png'

const Home = () => {
    return (
        <div>
            <NavBar title='Login' url='/login'/>
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                width: '90%',
                margin: '0 auto'
            }}>
                <div style={{
                    width: '60%',
                    height: '100vh',
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'center'
                }}>
                    <h6 style={{
                        color: 'green'
                    }}>#DIGITECH</h6>
                    <h2>Pelajari Skill baru serta mempersiapkan diri kamu untuk menjadi digital talent terbaik dengan Cinta Koding</h2>
                </div>

                <div style={{
                    width: '40%',
                    height: '100vh',
                    position: 'relative',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}>
                    <div
                        style={{
                            borderRadius: '47% 53% 53% 47% / 65% 46% 54% 35%',
                            backgroundColor: 'skyblue',
                            height: 280,
                            width: 250,
                            display: 'flex',
                            position: 'relative',
                        }}>
                        <img src={WorkSolution} style={{
                            borderRadius: '0 53% 53% 47% / 65% 46% 54% 35%',
                            height: 300,
                            width: 250,
                            display: 'flex',
                            position: 'absolute',
                            left: 0,
                            bottom: 0
                        }} alt="solution" />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Home