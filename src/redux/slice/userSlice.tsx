import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'

export interface IUser {
    id?: number,
    name?: string,
    username?: string,
    email?: string,
    address?: {
        street?: string,
        suite?: string,
        city?: string,
        zipcode?: string,
        geo?: {
            lat?: string,
            lng?: string
        }
    },
    phone?: string,
    website?: string,
    company?: {
        name?: string,
        catchPhrase?: string,
        bs?: string
    }
}

export interface IUserState {
    userDetail: IUser
    userLoggedIn: IUser
    users: IUser[]
}

const initialState: IUserState = {
    userDetail: {},
    userLoggedIn: {},
    users: [],
}

export const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        getUserDetailActions: (state, action: PayloadAction<IUser>) => {
            state.userDetail = action.payload
        },
        setUserLoggedIn: (state, action: PayloadAction<IUser>) => {
            state.userLoggedIn = action.payload
        },
        getUsersActions: (state, action: PayloadAction<IUser[]>) => {
            state.users = action.payload
        },
    },
})

const { actions, reducer } = userSlice
export const { getUserDetailActions, getUsersActions, setUserLoggedIn } = actions
export default reducer