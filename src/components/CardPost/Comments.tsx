import React from 'react'
import { IPostComments } from '../../redux/slice/postSlice'
import Gap from '../Gap'

const Comments = ({ comments }: { comments: IPostComments }) => {
    return (
        <div
            style={{
                display: 'flex',
                flexDirection: 'row'
            }}
        >
            <div style={{
                width: '30%'
            }}>
                <h6>{comments?.email.split('@')[0]}</h6>
            </div>
            <Gap width={30} />
            <div style={{
                width: '65%'
            }}>
                <p>{comments.body}</p>
            </div>
        </div>
    )
}

export default Comments