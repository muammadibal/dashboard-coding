import { getUserDetailServices, getUsersServices } from "../../services"
import { ISignal, IUserDetail } from "../../interface/services"
import { getUserDetailActions, getUsersActions } from "../slice/userSlice"

export const getUsers = ({ signal }: ISignal) => async (dispatch: any) => {
    try {
        const users = await getUsersServices({ signal })
        dispatch(getUsersActions(users))
        return users
    } catch (error) {
        return error
    }
}

export const getUserDetail = ({ id, signal }: IUserDetail) => async (dispatch: any) => {
    try {
        const user = await getUserDetailServices({ id, signal })
        dispatch(getUserDetailActions(user))
        return user
    } catch (error) {
        return error
    }
}