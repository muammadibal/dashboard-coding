import React from 'react'

interface IGap {
    height?: number,
    width?: number,
    styles?: React.CSSProperties | undefined
}

const Gap = ({ height, width, styles }: IGap) => {
    return (
        <div style={{ height, width, ...styles }} />
    )
}

export default Gap