# Cinta Koding

This project was for submission PT. One Code Solution.

## Steps to Clone Project

First, make sure the environtment have been setup in your machine

### `clone project`
```
git clone https://gitlab.com/muammadibal/dashboard-coding.git
```

### `install depedencies`
```
npm install
```

### `run project`
```
npm start
```

## .env example
```
REACT_APP_API_URL=
```

## Preview
|Landing                      |Login                      |Dashboard                       |
------------------------------|---------------------------|--------------------------------|
|![image](preview/landing.png)|![image](preview/login.png)|![image](preview/dashboard1.png)|

|Detail                          |Comment                         |User Detail                     |
|--------------------------------|--------------------------------|--------------------------------|
|![image](preview/dashboard2.png)|![image](preview/dashboard3.png)|![image](preview/dashboard4.png)|