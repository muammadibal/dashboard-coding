/* eslint-disable react-hooks/exhaustive-deps */
import { useFormik } from 'formik';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import * as yup from 'yup';
import { Button, Input } from '../../components';
import { getUsers } from '../../redux/actions';
import { IUser, setUserLoggedIn } from '../../redux/slice/userSlice';

const validationSchema = yup.object({
    username: yup
        .string()
        .required('Username is required'),
    password: yup
        .string()
        .required('Password Result Id is required'),
});

const Register = () => {
    const connUser = new AbortController()
    const dispatch = useDispatch()
    const { users } = useSelector((state: {
        user: {
            users: IUser[]
        }
    }) => state.user);

    useEffect(() => {
        dispatch<any>(getUsers({ signal: connUser.signal }))
    }, [])

    const formik = useFormik({
        initialValues: {
            username: '',
            password: '',
        },
        validationSchema: validationSchema,
        onSubmit: async (values, formikBag) => {
            const user = users.find(o => o.username?.toLowerCase() === values.username);
            if (user) {
                dispatch(setUserLoggedIn(user))
                window.location.href = '/dashboard'
            } else {
                formikBag.resetForm()
                toast.error("Username not found")
            }
        },
        enableReinitialize: true
    })

    return (
        <div style={{
            height: '100vh',
            width: '100%',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'column',
        }}>
            <div style={{
                height: '100vh',
                width: '30%',
                display: 'flex',
                alignContent: 'center',
                justifyContent: 'center',
                flexDirection: 'column',
            }}>
                <Input placeholder="" name='username' value={formik.values.username} onChange={formik.handleChange} label='Username' invalid={formik?.errors?.username ? true : false} errors={formik?.errors?.username} />
                <Input type='password' placeholder="" name='password' value={formik.values.password} onChange={formik.handleChange} label='Password' invalid={formik?.errors?.password ? true : false} errors={formik?.errors?.password} />
                <Button title='Login' onClick={formik.handleSubmit} />
            </div>
        </div>
    )
}

export default Register