/* eslint-disable react-hooks/exhaustive-deps */
import { ChangeEvent, useEffect, useMemo, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Input, NavBar } from '../../../components'
import { getPosts } from '../../../redux/actions/postActions'
import { IPost } from '../../../redux/slice/postSlice'
import { CardPost } from '../../../components'
import ReactPaginate from 'react-paginate';

const MainHome = () => {
  const connUser = new AbortController()
  const dispatch = useDispatch()
  const { posts } = useSelector((state: {
    post: {
      posts: IPost[]
    }
  }) => state.post);
  const [searchVal, setSearchVal] = useState<string>('')
  const [itemOffset, setItemOffset] = useState(0);
  let itemsPerPage = 5

  useEffect(() => {
    dispatch<any>(getPosts({ signal: connUser.signal }))
  }, [])

  const postsValue = useMemo(() => {
    const val = posts.filter(o =>
      o?.body?.toLowerCase().includes(searchVal.toLowerCase()),
    );
    return val;

  }, [searchVal])

  const endOffset = itemOffset + itemsPerPage;
  console.log(`Loading items from ${itemOffset} to ${endOffset}`);
  const currentItems = posts.slice(itemOffset, endOffset);
  const pageCount = Math.ceil(posts.length / itemsPerPage);

  const handlePageClick = (event: any) => {
    const newOffset = (event.selected * itemsPerPage) % posts.length;
    console.log(
      `User requested page number ${event.selected}, which is offset ${newOffset}`
    );
    setItemOffset(newOffset);
  };

  return (
    <div>
      <NavBar />

      <div
        style={{
          display: 'flex',
          height: '100vh',
          alignItems: 'center',
          flexDirection: 'column',
        }}>
        <Input value={searchVal} onChange={(e: ChangeEvent<HTMLInputElement>) => setSearchVal(e.target.value)} placeholder="Search..." />
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          {
            searchVal.length > 0 ?
              postsValue?.map((item: IPost, i: number) => {
                return <CardPost post={item} isShowDetail />
              }) : currentItems?.map((item: IPost, i: number) => {
                return <CardPost post={item} isShowDetail />
              })
          }
          {searchVal.length === 0 ?
            <ReactPaginate
              breakLabel="..."
              nextLabel={<i className='bx bxs-chevron-right'></i>}
              onPageChange={handlePageClick}
              marginPagesDisplayed={2}
              pageRangeDisplayed={5}
              pageCount={pageCount}
              previousLabel={<i className='bx bxs-chevron-left'></i>}
              breakClassName={"break-me"}
              containerClassName={"pagination"}
              // subContainerClassName={"pages pagination"}
              activeClassName={"active"}
            // renderOnZeroPageCount={null}
            />
            : null}
        </div>
      </div>
    </div>
  )
}

export default MainHome