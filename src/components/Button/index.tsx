import React from 'react'
import { Button as Btn } from 'reactstrap';
import { Link } from 'react-router-dom';

export interface IButton {
  isPrimary?: boolean
  title?: string
  url?: string
  onClick?: () => void
}
const Button = ({
  isPrimary = true,
  title = 'Button Title',
  url = '',
  onClick
}: IButton) => {

  if (url.length > 0) {
    return <Link to={url}>
      <Btn className='btn'
        style={{
          backgroundColor: isPrimary ? 'skyblue' : 'grey'
        }}
      >{title}</Btn>
    </Link>
  }
  return (
    <Btn className='btn'
      style={{
        backgroundColor: isPrimary ? 'skyblue' : 'grey'
      }}
      onClick={onClick}
    >{title}</Btn>
  )
}

export default Button