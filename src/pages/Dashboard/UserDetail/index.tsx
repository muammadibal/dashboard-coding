/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Table } from 'reactstrap';
import { NavBar } from '../../../components';
import { getUserDetail } from '../../../redux/actions';
import { IUser } from '../../../redux/slice/userSlice';
import { useNavigate, useParams } from 'react-router-dom';

const UserDetail = () => {
    const connUser = new AbortController()
    const dispatch = useDispatch()
    let { id } = useParams();
    const navigate = useNavigate();
    const { userDetail } = useSelector((state: {
        user: {
            userDetail: IUser
        }
    }) => state.user);

    useEffect(() => {
        dispatch<any>(getUserDetail({ id: id!.toString(), signal: connUser.signal }))
    }, [id])

    return (
        <div>
            <NavBar centerTitle='User' />

            <div style={{
                display: 'flex',
                height: '100vh',
                alignItems: 'flex-start',
                flexDirection: 'column',
            }}>
                <div style={{
                    width: '50%',
                    alignSelf: 'center',
                    cursor: 'pointer',
                    marginBottom: '1rem'
                }} onClick={() => {
                    navigate(-1)
                }}>
                    <i className='bx bxs-chevron-left bx-border bxs-lg'></i>
                </div>
                <Table hover style={{
                    width: '50%',
                    alignSelf: 'center'
                }}>
                    <tbody>
                        <tr>
                            <td>
                                Username
                            </td>
                            <td>
                                :
                            </td>
                            <th scope="row">
                                {userDetail?.username}
                            </th>
                        </tr>
                        <tr>
                            <td>
                                Email
                            </td>
                            <td>
                                :
                            </td>
                            <th scope="row">
                                {userDetail?.email}
                            </th>
                        </tr>
                        <tr>
                            <td>
                                Address
                            </td>
                            <td>
                                :
                            </td>
                            <th scope="row">
                                {userDetail?.address?.street}
                            </th>
                        </tr>
                        <tr>
                            <td>
                                Phone
                            </td>
                            <td>
                                :
                            </td>
                            <th scope="row">
                                {userDetail?.phone}
                            </th>
                        </tr>
                    </tbody>
                </Table>
            </div>
        </div>
    )
}

export default UserDetail