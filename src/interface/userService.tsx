import { ISignal } from "./services";

export interface IUserDetail extends ISignal {
    id: string;
}