import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'

export interface IPost {
    userId?: number,
    id?: number,
    title?: string,
    body?: string
}

export interface IPostComments {
    postId: number,
    id: number,
    name: string,
    email: string,
    body: string
}

export interface IPostState {
    postDetail: IPost
    postComments: IPostComments[]
    posts: IPost[]
}

const initialState: IPostState = {
    postDetail: {},
    postComments: [],
    posts: [],
}

export const userSlice = createSlice({
    name: 'post',
    initialState,
    reducers: {
        getPostsActions: (state, action: PayloadAction<IPost[]>) => {
            state.posts = action.payload
        },
        getPostDetailActions: (state, action: PayloadAction<IPost>) => {
            state.postDetail = action.payload
        },
        getPostCommentsActions: (state, action: PayloadAction<IPostComments[]>) => {
            state.postComments = action.payload
        },
    },
})

const { actions, reducer } = userSlice
export const { getPostsActions, getPostDetailActions, getPostCommentsActions } = actions
export default reducer