import React from 'react'
import { useSelector } from 'react-redux';
import { Link, useLocation } from 'react-router-dom';
import { Button } from '../../components'
import { IUser } from '../../redux/slice/userSlice';
import './styles.css'
interface INavBar {
    title?: string
    url?: string
    centerTitle?: string
}

const NavBar = ({ centerTitle = "Post", title = "Button", url = '' }: INavBar) => {
    const location = useLocation();
    const { userLoggedIn } = useSelector((state: {
        user: {
            userLoggedIn: IUser
        }
    }) => state.user);

    const isUrlDashboard = location.pathname.split('/').includes('dashboard');

    return (
        <div style={{
            display: 'flex',
            // position: 'absolute',
            // top: 0,
            // left: 0,
            // right: 0,
            backgroundColor: 'white',
            height: 80,
            alignItems: 'center',
            justifyContent: 'center',
            // zIndex: 99
        }}>
            <div
                style={{
                    width: '80%',
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                }}>
                <h4>Cinta Coding</h4>

                {isUrlDashboard ? <h6 style={{
                    borderBottom: '2px solid skyblue'
                }}>{centerTitle}</h6> : null}

                {isUrlDashboard ?
                    <div className="dropdown">
                        <h6 className="dropbtn">WelCome, <span style={{ color: 'skyblue' }}>{userLoggedIn?.username}</span></h6>
                        <div className="dropdown-content">
                            <Link to={`/dashboard/user/${userLoggedIn?.id}`}>Detail Profile</Link>
                        </div>
                    </div>
                    :
                    <Button title={title} url={url} />}
            </div>
        </div>
    )
}

export default NavBar